package hadhunter.token.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ValidationException;
import java.beans.Transient;
import java.util.Date;
import java.util.Objects;

@JsonSerialize
@JsonDeserialize
public class Token {

    @JsonProperty("token")
    private String token;

    @JsonProperty("id")
    private String id;

    @JsonProperty("date")
    private Date date;

    private transient boolean isUsed;

    public Token(String token) {
        this.token = token;
    }

    public Token() {
    }

    public Token(Token token) {
        this.token = token.token;
        this.id = token.id;
        this.isUsed = token.isUsed;
        this.date = token.date;
    }

    public Token(String token, String id, boolean isUsed, Date date) {
        this.token = token;
        this.id = id;
        this.isUsed = isUsed;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken() {
        this.token = token;
    }

    public boolean isUsed() {
        return isUsed;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Token tokenToCompare = (Token) obj;
        return Objects.equals(token, tokenToCompare.token);
    }

    public void validateToken() {
        if (StringUtils.isEmpty(token)) {
            throw new ValidationException("Token must be not-null");
        }
    }

    private void validateId() {
        if (StringUtils.isEmpty(id)) {
            throw new ValidationException("ID must be not-null");
        }
    }

    public void validate() {
        validateId();
        validateToken();
    }

    public void setUsedFlagTrue() {
        isUsed = true;
    }

    @Override
    public String toString() {
        return "Token{" +
                "token='" + token + '\'' +
                '}';
    }


}

package hadhunter.token.tokencontroller;

import hadhunter.token.exception.TokenDatabaseServiceException;
import hadhunter.token.exception.TokenStorageException;
import hadhunter.token.model.Token;
import hadhunter.token.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tokens")
public class TokenController {

    private final TokenService tokenService;

    @Autowired
    public TokenController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @GetMapping("/newToken")
    ResponseEntity addToken() throws TokenDatabaseServiceException {
        Token newToken = tokenService.generateToken();
        return ResponseEntity.ok(newToken);
    }

    @PostMapping("/checkToken")
    ResponseEntity checkToken(@RequestBody Token token) throws TokenStorageException {
        return ResponseEntity.ok(tokenService.check(token));
    }

}

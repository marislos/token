package hadhunter.token.tokencontroller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
class TokenNotFoundException extends RuntimeException {
    public TokenNotFoundException() {
        super("Token does not exist");
    }
}

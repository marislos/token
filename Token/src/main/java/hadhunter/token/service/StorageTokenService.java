package hadhunter.token.service;

import hadhunter.token.db.DatabaseTokenService;
import hadhunter.token.exception.TokenDatabaseServiceException;
import hadhunter.token.exception.TokenStorageException;
import hadhunter.token.model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class StorageTokenService {

    private DatabaseTokenService databaseTokenService;

    @Autowired
    public StorageTokenService(DatabaseTokenService databaseTokenService) {
        this.databaseTokenService = databaseTokenService;
    }

    public void saveNewToken(@NotNull Token token) throws TokenDatabaseServiceException {
        token.validateToken();
        try {
            databaseTokenService.saveNewToken(token);
        } catch (TokenDatabaseServiceException e) {
            throw new TokenDatabaseServiceException("This token was not saved");
        }
    }

    public Token getToken(@NotNull Token token) throws TokenStorageException {
        try {
            Token savedToken = databaseTokenService.getUnusedToken(token);
            if (savedToken != null) {
                return savedToken;
            }
            savedToken = databaseTokenService.getUsedToken(token);
            if (savedToken != null) {
                savedToken.setUsedFlagTrue();
                return savedToken;
            }
        } catch (TokenDatabaseServiceException e) {
            throw new TokenStorageException("There is an error of finding the token: " + token);
        }

        throw new TokenStorageException("The token is not found");
    }

    void updateToken(@NotNull Token token) throws TokenStorageException {
        try {
            databaseTokenService.updateToken(token);
        } catch (TokenDatabaseServiceException e) {
            throw new TokenStorageException("There is an error to update token");
        }
    }

    public void deleteToken(@NotNull Token token) throws TokenDatabaseServiceException {
        try {
            databaseTokenService.deleteUsedToken(token);
        } catch (TokenDatabaseServiceException e) {
            throw new TokenDatabaseServiceException("This token was not deleted");
        }
    }
}

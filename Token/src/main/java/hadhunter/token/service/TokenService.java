package hadhunter.token.service;

import hadhunter.token.exception.TokenDatabaseServiceException;
import hadhunter.token.exception.TokenStorageException;
import hadhunter.token.model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class TokenService {

    private StorageTokenService storageTokenService;

    @Autowired
    public TokenService(StorageTokenService storageTokenService) {
        this.storageTokenService = storageTokenService;
    }

    public boolean check(Token checkToken) throws TokenStorageException {
        try {
            Token savedToken = storageTokenService.getToken(checkToken);
            if (!savedToken.isUsed()) {
                savedToken.setId(checkToken.getId());
                savedToken.setDate(new Date());
                storageTokenService.updateToken(savedToken);
            }
            return true;
        } catch (TokenStorageException e) {
            throw new TokenStorageException("Token not found");
        }
    }
    public Token generateToken() throws TokenDatabaseServiceException {
        Token newToken = new Token(UUID.randomUUID().toString());
        storageTokenService.saveNewToken(newToken);
        return newToken;
    }

}

package hadhunter.token.db;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import hadhunter.token.exception.TokenDatabaseServiceException;

import java.io.FileInputStream;
import java.util.Objects;


public class FirebaseInitializer {

    private static final String DATABASE_URL = "https://digitalcalendarhh.firebaseio.com";
    private static final String DATABASE_ACCOUNT_FILE = "digitalcalendarhh-firebase-adminsdk-5vbhh-71dfc5d268.json";

    private FirebaseInitializer() {
        throw new RuntimeException("No instance of this class available");
    }

    public static Firestore getDb() throws TokenDatabaseServiceException {
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(Objects.requireNonNull(classloader.getResourceAsStream(DATABASE_ACCOUNT_FILE))))
                    .setDatabaseUrl(DATABASE_URL)
                    .build();
            FirebaseApp.initializeApp(options);
            return FirestoreClient.getFirestore();
        } catch (Exception e) {
            throw new TokenDatabaseServiceException("There is an error to configure database connection", e);
        }
    }
}

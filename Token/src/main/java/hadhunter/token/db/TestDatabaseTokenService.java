package hadhunter.token.db;

import hadhunter.token.exception.TokenDatabaseServiceException;
import hadhunter.token.model.Token;

import java.util.*;

//@Component
public class TestDatabaseTokenService implements DatabaseTokenService {

    private static Set<Token> tokens = new HashSet<>();
    private static Map<String, Token> tokensId = new HashMap<>();

    @Override
    public void saveNewToken(Token token) {
        tokens.add(token);
    }

    @Override
    public Token getUnusedToken(Token token) {
        if (tokens.contains(token)) {
            return new Token(token);
        }
        return null;
    }

    @Override
    public Token getUsedToken(Token token) {
        return tokensId.get(token.getId());
    }

    @Override
    public void deleteUsedToken(Token token) {
        tokensId.remove(token.getId());
    }

    @Override
    public void updateToken(Token token) throws TokenDatabaseServiceException {
    }

    @Override
    public List<Token> getExpiredTokens() throws TokenDatabaseServiceException {
        return null;
    }
}

package hadhunter.token.db;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import hadhunter.token.exception.TokenDatabaseServiceException;
import hadhunter.token.model.Token;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Component
public class FirebaseDatabaseTokenService implements DatabaseTokenService {

    private static final String ID_FIELD_NAME = "id";
    private static final String TOKEN_FIELD_NAME = "token";
    private static final String DATE_FIELD_NAME = "date";

    private final Firestore firestore;

    public FirebaseDatabaseTokenService() throws TokenDatabaseServiceException {
        firestore = FirebaseInitializer.getDb();
    }

    @Override
    public void saveNewToken(Token token) throws TokenDatabaseServiceException {
        WriteBatch batch = firestore.batch();
        DocumentReference ref = firestore.collection("new_tokens").document(token.getToken());
        batch.set(ref, token);
        ApiFuture<List<WriteResult>> result = batch.commit();
        if (result.isCancelled()) {
            throw new TokenDatabaseServiceException("There is an error to save the token");
        }
    }


    @Override
    public Token getUnusedToken(Token token) throws TokenDatabaseServiceException {
        try {
            DocumentSnapshot ref = firestore.collection("new_tokens").document(token.getToken()).get().get();
            if (ref.exists()) {
                return new Token(ref.getString(TOKEN_FIELD_NAME), null, false, null);
            }
        } catch (InterruptedException | ExecutionException e) {
            throw new TokenDatabaseServiceException("There is an error to receive the token from DB", e);
        }
        throw new TokenDatabaseServiceException("The token is not found");
    }

    @Override
    public Token getUsedToken(Token token) throws TokenDatabaseServiceException {
        try {
            DocumentSnapshot ref = firestore.collection("used_tokens").document(token.getId()).get().get();
            if (ref.exists()) {
                return new Token(ref.getString(TOKEN_FIELD_NAME), ref.getString(ID_FIELD_NAME), true, ref.getDate(DATE_FIELD_NAME));
            }
        } catch (InterruptedException | ExecutionException e) {
            throw new TokenDatabaseServiceException("There is an error to receive the token from DB", e);
        }
        throw new TokenDatabaseServiceException("The token is not found");
    }

    @Override
    public void deleteUsedToken(Token token) throws TokenDatabaseServiceException {
        WriteBatch batch = firestore.batch();
        DocumentReference ref = firestore.collection("used_tokens").document(token.getId());
        batch.delete(ref);
        ApiFuture<List<WriteResult>> result = batch.commit();
        if (result.isCancelled()) {
            throw new TokenDatabaseServiceException("There is an error to delete the used token");
        }

    }

    @Override
    public void updateToken(Token token) throws TokenDatabaseServiceException {
        WriteBatch batch = firestore.batch();
        DocumentReference saveTokenDocRef = firestore.collection("used_tokens").document(token.getId());
        DocumentReference deleteTokenDocRef = firestore.collection("new_tokens").document(token.getToken());
        batch.set(saveTokenDocRef, token);
        batch.delete(deleteTokenDocRef);
        ApiFuture<List<WriteResult>> result = batch.commit();
        if (result.isCancelled()) {
            throw new TokenDatabaseServiceException("There is an error to update the token");
        }
    }

    @Override
    public List<Token> getExpiredTokens() throws TokenDatabaseServiceException {
        List<QueryDocumentSnapshot> documents;
        try {
            Date dayBefore = DateUtils.addDays(new Date(), -1);
            documents = firestore.collection("used_tokens").whereLessThan("date", dayBefore).get().get().getDocuments();
        } catch (InterruptedException | ExecutionException e) {
            throw new TokenDatabaseServiceException("These tokens not founded", e);
        }
        return documents.stream()
                .map(doc -> new Token(doc.getString(TOKEN_FIELD_NAME),
                        doc.getString(ID_FIELD_NAME),
                        true,
                        doc.getDate(DATE_FIELD_NAME)))
                .collect(Collectors.toList());
    }
}

package hadhunter.token.db;

import hadhunter.token.exception.TokenDatabaseServiceException;
import hadhunter.token.model.Token;

import java.util.List;

public interface DatabaseTokenService {

    void saveNewToken(Token token) throws TokenDatabaseServiceException;

    Token getUnusedToken(Token token) throws TokenDatabaseServiceException;

    Token getUsedToken(Token token) throws TokenDatabaseServiceException;

    void deleteUsedToken(Token token) throws TokenDatabaseServiceException;

    void updateToken(Token token) throws TokenDatabaseServiceException;

    List<Token> getExpiredTokens() throws TokenDatabaseServiceException;
}

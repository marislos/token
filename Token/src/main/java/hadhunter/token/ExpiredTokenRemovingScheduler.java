package hadhunter.token;

import hadhunter.token.db.DatabaseTokenService;
import hadhunter.token.exception.TokenDatabaseServiceException;
import hadhunter.token.model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ExpiredTokenRemovingScheduler {

    private DatabaseTokenService databaseTokenService;

    @Autowired
    public ExpiredTokenRemovingScheduler(DatabaseTokenService databaseTokenService) {
        this.databaseTokenService = databaseTokenService;
    }

    @Scheduled(fixedRate = 1_800_000)
    public void reportCurrentData() throws TokenDatabaseServiceException {
        try {
            for (Token tokenUsed : databaseTokenService.getExpiredTokens()) {
                databaseTokenService.deleteUsedToken(tokenUsed);
            }
        } catch (TokenDatabaseServiceException e) {
            throw new TokenDatabaseServiceException("There is an error to receive the token from DB", e);
        }
    }
}

package hadhunter.token.exception;

public class TokenStorageException extends Exception {

    public TokenStorageException() {
    }

    public TokenStorageException(String message) {
        super(message);
    }

    public TokenStorageException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenStorageException(Throwable cause) {
        super(cause);
    }
}

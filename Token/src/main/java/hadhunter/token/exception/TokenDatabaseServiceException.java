package hadhunter.token.exception;

/**
 * Ошибка при обращении к БД при сохранении/удалении/обновлении токена
 */
public class TokenDatabaseServiceException extends Exception {

    public TokenDatabaseServiceException() {
    }

    public TokenDatabaseServiceException(String message) {
        super(message);
    }

    public TokenDatabaseServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenDatabaseServiceException(Throwable cause) {
        super(cause);
    }
}
